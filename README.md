# README #

This example demonstrates the use of retrofit in an android application.

To run this project build the server application, which is in my github repository:
[https://github.com/merkury/mobilecloud-15/tree/master/examples/3-VideoControllerAndRetrofit
](https://github.com/merkury/mobilecloud-15/tree/master/examples/3-VideoControllerAndRetrofit
)

./gradlew build -x test
or 
./gradlew bootRun

Postman: 
Content-type: application/json
post body (raw):
{
  "name" : "bla" ,
  "url" : "test" ,
  "duration" : "10"
}



This is a simple spring-boot application. You can find more about spring boot here:
[https://spring.io/guides/gs/spring-boot/](https://spring.io/guides/gs/spring-boot/)

If you found a bug or some other issues, please contact me!