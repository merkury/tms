package de.htw_berlin.retrofitclientsample


/**
 * A simple object to represent a video and its URL for viewing.
 *
 * @author jules
 */
class Video(name: String, url: String, private var duration: Long) {

    var name: String? = name
    private var url: String? = url

    override fun toString(): String {
        return "$name $url $duration"
    }
}
