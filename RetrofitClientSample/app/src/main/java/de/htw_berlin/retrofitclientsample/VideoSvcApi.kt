package de.htw_berlin.retrofitclientsample

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface VideoSvcApi {

    @get:GET(VIDEO_SVC_PATH)
    val videoList: Call<List<Video>>

    @POST(VIDEO_SVC_PATH)
    fun addVideo(@Body v: Video): Call<Boolean>

    companion object {

        // The path where we expect the VideoSvc to live
        const val VIDEO_SVC_PATH = "/video"
    }

}
