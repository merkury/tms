package de.htw_berlin.retrofitclientsample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PostVideoActivity : AppCompatActivity(), Callback<Boolean>, View.OnClickListener {
    private var api: VideoSvcApi? = null

    private var videoName: EditText? = null
    private var videoUrl: EditText? = null
    private var videoDuration: EditText? = null
    private var sendButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_video)

        sendButton = findViewById(R.id.send)
        sendButton!!.setOnClickListener(this)

        videoName = findViewById(R.id.et_videoname)
        videoUrl = findViewById(R.id.et_videourl)
        videoDuration = findViewById(R.id.et_videoduration)

        val retrofit = Retrofit.Builder()
                .baseUrl(MainActivity.baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        // prepare call in Retrofit 2.0
        api = retrofit.create(VideoSvcApi::class.java)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.send) {
            sendButton!!.isClickable = false
            val name = videoName!!.text.toString().trim { it <= ' ' }
            val url = videoUrl!!.text.toString().trim { it <= ' ' }
            val durationStr = videoDuration!!.text.toString().trim { it <= ' ' }

            if (name.isEmpty() || url.isEmpty() || durationStr.isEmpty()) {
                Toast.makeText(this, "enter some data", Toast.LENGTH_SHORT).show()
                sendButton!!.isClickable = true
                return
            }

            var duration: Long = 0

            if (durationStr.isNotEmpty()) {
                duration = java.lang.Long.parseLong(durationStr)
            }

            val video = Video(name, url, duration)

            val call = api!!.addVideo(video)
            //asynchronous call
            call.enqueue(this)
        }
    }

    override fun onResponse(call: Call<Boolean>, response: Response<Boolean>) {
        sendButton!!.isClickable = true
        if (null != response.body()) {
            val success = response.body()
            Toast.makeText(this, "added: " + success!!, Toast.LENGTH_LONG).show()
        }
    }

    override fun onFailure(call: Call<Boolean>, t: Throwable) {
        sendButton!!.isClickable = true
        Toast.makeText(this, "failed", Toast.LENGTH_LONG).show()
    }
}
