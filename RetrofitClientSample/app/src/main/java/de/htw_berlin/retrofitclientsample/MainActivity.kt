package de.htw_berlin.retrofitclientsample

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
//import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.Toast

import java.util.ArrayList

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), Callback<List<Video>>, View.OnClickListener {

    private var api: VideoSvcApi? = null
    private var refreshButton: Button? = null
    private var arrayAdapter: ArrayAdapter<Video>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Deprecated:
        //requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS)
        //requestWindowFeature(Window.FEATURE_PROGRESS)

        setContentView(R.layout.activity_main)

        refreshButton = findViewById(R.id.btnrefresh)
        refreshButton!!.setOnClickListener(this)

        val addButton = findViewById<Button>(R.id.addVideo)
        addButton.setOnClickListener(this)

        val listview = findViewById<ListView>(R.id.listView)

        arrayAdapter = ArrayAdapter(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                ArrayList<Video>())
        listview.adapter = arrayAdapter

        //setProgressBarIndeterminateVisibility(true)
        //setProgressBarVisibility(true)

        val retrofit = Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        // prepare call in Retrofit 2.0
        api = retrofit.create<VideoSvcApi>(VideoSvcApi::class.java)

        val call = api!!.videoList
        //asynchronous call
        call.enqueue(this)
    }

    override fun onResponse(call: Call<List<Video>>, response: Response<List<Video>>) {
        refreshButton!!.isClickable = true
        val len = response.body()?.size ?: 0
        Toast.makeText(this, "videolist: " + len, Toast.LENGTH_LONG).show()
        val videolist = response.body()

        //setProgressBarIndeterminateVisibility(false)
        arrayAdapter!!.clear()
        arrayAdapter!!.addAll(videolist!!)
        arrayAdapter!!.notifyDataSetChanged()

    }

    override fun onFailure(call: Call<List<Video>>, t: Throwable) {
        refreshButton!!.isClickable = true
        Toast.makeText(this, "failure", Toast.LENGTH_LONG).show()
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btnrefresh) {
            val call = api!!.videoList
            //asynchronous call
            call.enqueue(this)
            v.isClickable = false
        } else if (v.id == R.id.addVideo) {
            val intent = Intent(this, PostVideoActivity::class.java)
            startActivity(intent)
        }
    }

    companion object { // TODO change to your host!
        val baseURL = "http://192.168.2.102:8080"  //http://192.168.122.1:8080"
    }
}
